package com.example.demouala.service.libros;

import com.example.demouala.model.Libro;
import com.example.demouala.service.IRetrofitInstance;
import com.example.demouala.service.RetrofitInstance;
import com.example.demouala.ui.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BooksService implements IGenericService<Libro> {

    @Inject
    RetrofitInstance retrofitInstance;

    public BooksService() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void doGetRequest(IResponseListener<Libro> listener) {
        IRetrofitInstance service = retrofitInstance.createService(IRetrofitInstance.class);

        Call<ArrayList<Libro>> call = service.getBooks();

        call.enqueue(new Callback<ArrayList<Libro>>() {
            @Override
            public void onResponse(Call<ArrayList<Libro>> call, Response<ArrayList<Libro>> response) {
                if (response.isSuccessful()) {
                    listener.onResponseSuccess(response.body());
                } else {
                    listener.onResponseError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Libro>> call, Throwable t) {
                listener.onResponseError(-1, t.getMessage());
            }
        });
    }
}
