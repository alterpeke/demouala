package com.example.demouala.service.libros;

import java.util.ArrayList;

public interface IGenericService<T> {

    void doGetRequest(IGenericService.IResponseListener<T> listener);

    interface IResponseListener<T> {
        void onResponseSuccess(ArrayList<T> objects);
        void onResponseError(int errorCode, String message);
    }

}
