package com.example.demouala.service;

import com.example.demouala.model.Libro;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IRetrofitInstance {

    @GET("books")
    Call<ArrayList<Libro>> getBooks();

}
