package com.example.demouala.model;


import com.google.gson.annotations.SerializedName;

public class Libro {

    @SerializedName("id")
    private Long id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("autor")
    private String autor;
    @SerializedName("disponibilidad")
    private Boolean disponibilidad;
    @SerializedName("popularidad")
    private Long popularidad;
    @SerializedName("imagen")
    private String imagen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Long getPopularidad() {
        return popularidad;
    }

    public void setPopularidad(Long popularidad) {
        this.popularidad = popularidad;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

}
