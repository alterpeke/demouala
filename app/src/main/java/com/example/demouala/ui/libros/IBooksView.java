package com.example.demouala.ui.libros;

import com.example.demouala.model.Libro;

import java.util.ArrayList;

public interface IBooksView {

    void dataLoaded(ArrayList<Libro> libros);
    void showError(int errorCode, String message);

}
