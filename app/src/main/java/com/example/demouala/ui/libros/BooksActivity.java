package com.example.demouala.ui.libros;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demouala.R;
import com.example.demouala.model.Libro;
import com.example.demouala.ui.adapter.BooksViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BooksActivity extends AppCompatActivity implements IBooksView {

    private BooksPresenter presenter;
    private BooksViewAdapter adapter;

    private ArrayList<Libro> libros = new ArrayList<>();

    @BindView(R.id.rv_lista)
    RecyclerView recyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        RecyclerView.LayoutManager layoutManagerSelected= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManagerSelected);

        presenter = new BooksPresenter(this);
        presenter.loadBooks();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onFinalize();
    }

    @Override
    public void dataLoaded(ArrayList<Libro> libros) {
        this.libros = libros;
        sortBooks(this.libros);
        adapter = new BooksViewAdapter(this.libros, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError(int errorCode, String message) {
        Toast.makeText(this, R.string.errorCarga + message, Toast.LENGTH_SHORT).show();
    }

    public void sortBooks(ArrayList<Libro> libros){
        Collections.sort(libros, new Comparator<Libro>() {
            @Override
            public int compare(Libro l1, Libro l2) {
                return (l1.getPopularidad().compareTo(l2.getPopularidad()));
            }
        });
    }
}
