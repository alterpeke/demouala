package com.example.demouala.ui.libros;

import com.example.demouala.model.Libro;

import java.util.ArrayList;

public interface IBooksInteractor {

    void getLibros(IGetNotesListener listener);
    void onFinalize();

    interface IGetNotesListener{
        void onLoaded(ArrayList<Libro> libros);
        void showError(int errorCode, String Message);
        void onFinalize();
    }
}
